sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (Controller, MessageBox) {
	"use strict";

	return Controller.extend("com.cg.coe.quizmaster.zquizmaster.controller.View1", {
		onInit: function () {
			this.oModel = this.getOwnerComponent().getModel();
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		},
		onAdd: function () {
			this.oRouter.navTo("view2", {
				objectId: "new"
			});
		},
		onEdit: function (oEvent) {
			this.oRouter.navTo("view2", {
				objectId: oEvent.getSource().getBindingContext().getProperty("questionId")
			});
		},
		onJobEdit: function (oEvent) {
			this.oRouter.navTo("JobEdit", {
				objectId: oEvent.getSource().getBindingContext().getProperty("id")
			});
		},
		onJobAdd: function () {
			this.oRouter.navTo("JobEdit", {
				objectId: "new"
			});
		},
		onTopicEdit: function (oEvent) {
			this.oRouter.navTo("TopicEdit", {
				objectId: oEvent.getSource().getBindingContext().getProperty("id")
			});
		},
		onTopicAdd: function () {
			this.oRouter.navTo("TopicEdit", {
				objectId: "new"
			});
		},
		onFileChange: function (oEvent) {
			debugger;
			var uploadUrl = "/destination/capgemini/apps/coe/gamification/services/QuizQuestionUpload.xsjs";
			var fileLoader = oEvent.getSource();
			//Method 1
			/*if (!this.csrfToken) {
				this.csrfToken = this.oModel.oHeaders["x-csrf-token"];
				fileLoader.setSendXHR(true);
				var oHeaderParm = new sap.ui.unified.FileUploaderParameter();
				oHeaderParm.setName('x-csrf-token');
				oHeaderParm.setValue(this.csrfToken);
				fileLoader.addHeaderParameter(oHeaderParm);
			}
			fileLoader.setUploadUrl(url);
			fileLoader.upload();*/

			//Method 2
			var oFiles = oEvent.getParameters().files;
			var fd = new window.FormData();
			fd.append("files", oFiles[0], oFiles[0].name);
			var _csrfToken = this.oModel.oHeaders["x-csrf-token"];
			$.ajax({
				url: uploadUrl,
				type: "POST",
				processData: false,
				contentType: false,
				data: fd,
				beforeSend: function (xhr) {
					xhr.setRequestHeader("X-CSRF-Token", _csrfToken);
				},
				success: function (data, textStatus, XMLHttpRequest) {
					debugger;
				},
				error: function (oError, textStatus) {
					debugger;
					MessageBox.show("File could not be uploaded.", MessageBox.Icon.ERROR, "Error");
				}
			});

			//Method 3
			/*$.ajax({
				url: uploadUrl,
				type: "GET",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("X-CSRF-Token", "Fetch");
				},
				success: function (data, textStatus, XMLHttpRequest) {
					var token = XMLHttpRequest.getResponseHeader('X-CSRF-Token');
					$.ajax({
						url: uploadUrl,
						type: "POST",
						processData: false,
						contentType: false,
						data: fd,
						beforeSend: function (xhr) {
							xhr.setRequestHeader("X-CSRF-Token", token);
						},
						success: function (data, textStatus, XMLHttpRequest) {
							debugger;
						},
						error: function (oError, textStatus) {
							debugger;
							MessageBox.show("File could not be uploaded.", MessageBox.Icon.ERROR, "Error");
						}
					});
				},
				error: function (oError, textStatus) {
					debugger;
					MessageBox.show("File could not be uploaded.", MessageBox.Icon.ERROR, "Error");
				}
			});*/

		},
		onFileUploadComplete: function (oEvent) {

		}
	});
});