sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (Controller, History, MessageBox) {
	"use strict";

	return Controller.extend("com.cg.coe.quizmaster.zquizmaster.controller.view2", {

		onInit: function () {
			this.oModel = this.getOwnerComponent().getModel();
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._ztagbox = this.getView().byId("ztagbox");
			this._ztagboxItem = this.getView().byId("idVbTag").clone();
			this._oViewModel = new sap.ui.model.json.JSONModel({
				rb1Text: "",
				rb2Text: "",
				rb3Text: "",
				rb4Text: "",
			});
			this.getView().setModel(this._oViewModel, "viewModel");
			this.oRouter.getRoute("view2").attachPatternMatched(this._onObjectMatched, this);
		},
		onBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this._oRouter.navTo("TargetView1", true);
			}
		},
		_onObjectMatched: function (oEvent) {
			this._oObjectId = oEvent.getParameter("arguments").objectId;
			if (this._oObjectId === "new") {
				this.oModel.setDeferredBatchGroups(["createGroup"]);

				this._oNewContext = this.oModel.createEntry("/QUIZQUESTIONS", {
					groupId: "createGroup",
					properties: {
						questionId: "0",
						question: "",
						option1: "",
						option2: "",
						option3: "",
						option4: "",
						correctAns: "",
						topic: "",
						hint: "",
						complexity: "",
						createdOn: new Date()
					}
				});
				this._oObjectPath = this._oNewContext.sPath;
				//this.getView().setBindingContext(this._oNewContext);
				this.getView().bindElement({
					path: this._oObjectPath
				});
			} else {
				this._oObjectPath = "/QUIZQUESTIONS('" + this._oObjectId + "')";
				this.getView().bindElement({
					path: this._oObjectPath
				});
			}

			/*bind tags*/
			this._ztagbox.bindAggregation("items", "/QUIZTAGS", this._ztagboxItem);

			var oContent = this._ztagbox.getBinding("items");
			oContent.attachDataReceived(function (oData) {
				if (this._oObjectId !== "new") {
					var _topic = this.oModel.getProperty(this._oObjectPath).topic;
					
					for (var i = 0; i < this._ztagbox.getItems().length; i++) {
						var _cb = this._ztagbox.getItems()[i].getAggregation("items")[0];
						if (_topic!== "" && _cb.getCustomData()[0].getValue().toString() === _topic) {
							_cb.setSelected(true);
						}
					}
				}
			}.bind(this));

			var _oView = this.getView();
			$(".zoptioninput").mouseover(function () {
				if (this.id.indexOf('idOptionInput1') !== -1 && _oView.byId("idRbl1").getText() !== "Answer") {
					_oView.byId("idRbl1").setText("mark as answer");
				} else if (this.id.indexOf('idOptionInput2') !== -1 && _oView.byId("idRbl2").getText() !== "Answer") {
					_oView.byId("idRbl2").setText("mark as answer");
				} else if (this.id.indexOf('idOptionInput3') !== -1 && _oView.byId("idRbl3").getText() !== "Answer") {
					_oView.byId("idRbl3").setText("mark as answer");
				} else if (this.id.indexOf('idOptionInput4') !== -1 && _oView.byId("idRbl4").getText() !== "Answer") {
					_oView.byId("idRbl4").setText("mark as answer");
				}
			});
			$(".zoptioninput").mouseout(function () {
				if (this.id.indexOf('idOptionInput1') !== -1 && _oView.byId("idRbl1").getText() !== "Answer") {
					_oView.byId("idRbl1").setText("");
				} else if (this.id.indexOf('idOptionInput2') !== -1 && _oView.byId("idRbl2").getText() !== "Answer") {
					_oView.byId("idRbl2").setText("");
				} else if (this.id.indexOf('idOptionInput3') !== -1 && _oView.byId("idRbl3").getText() !== "Answer") {
					_oView.byId("idRbl3").setText("");
				} else if (this.id.indexOf('idOptionInput4') !== -1 && _oView.byId("idRbl4").getText() !== "Answer") {
					_oView.byId("idRbl4").setText("");
				}
			});

			/*Set answer selected*/
			var _correctAns = this.oModel.getProperty(this._oObjectPath).correctAns;
			if (_correctAns === "") {
				_oView.byId("idRbl1").setSelected(false);
				_oView.byId("idRbl2").setSelected(false);
				_oView.byId("idRbl3").setSelected(false);
				_oView.byId("idRbl4").setSelected(false);
				this._resetAns();
			} else if (_correctAns !== "" && _correctAns === this.oModel.getProperty(this._oObjectPath).option1) {
				_oView.byId("idRbl1").setSelected(true);
				_oView.byId("idRbl1").fireSelect();
			} else if (_correctAns !== "" && _correctAns === this.oModel.getProperty(this._oObjectPath).option2) {
				_oView.byId("idRbl2").setSelected(true);
				_oView.byId("idRbl2").fireSelect();
			} else if (_correctAns !== "" && _correctAns === this.oModel.getProperty(this._oObjectPath).option3) {
				_oView.byId("idRbl3").setSelected(true);
				_oView.byId("idRbl3").fireSelect();
			} else if (_correctAns !== "" && _correctAns === this.oModel.getProperty(this._oObjectPath).option4) {
				_oView.byId("idRbl4").setSelected(true);
				_oView.byId("idRbl4").fireSelect();
			}
		},
		onSelectAns: function (oEvent) {
			var _ansValue = oEvent.getSource().getCustomData()[0].getValue();
			if (_ansValue === "") {
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.error(
					"Please provide options first.", {
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
				oEvent.getSource().setSelected(false);
				this._resetAns();
			} else {
				this.oModel.setProperty(this._oObjectPath + "/correctAns", _ansValue);
				this._resetAns();
				oEvent.getSource().setText("Answer");
			}
		},
		_resetAns: function () {
			this._oViewModel.setProperty("/rb1Text", "");
			this._oViewModel.setProperty("/rb2Text", "");
			this._oViewModel.setProperty("/rb3Text", "");
			this._oViewModel.setProperty("/rb4Text", "");
		},
		onCBSelect: function (oEvent) {
			var bValid = true;
			var bSelected = oEvent.getSource().getSelected();
			var _sSelectedTag = oEvent.getSource().getCustomData()[0].getValue().toString();
			var _sTopic = this.oModel.getProperty(this._oObjectPath + "/topic");
			if (bSelected) {
				if (_sTopic === "") {
					this.oModel.setProperty(this._oObjectPath + "/topic", _sSelectedTag);
				} else {
					bValid = false;
				}
			} else {
				if (_sTopic !== "") {
					this.oModel.setProperty(this._oObjectPath + "/topic", "");
				}
			}
			if (!bValid) {
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.information(
					"You can select One tags only.", {
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
				oEvent.getSource().setSelected(false);
			}
		},
		onSave: function () {
			this.oModel.submitChanges({
				groupId: "createGroup",
				success: this._onSubmitSuccess.bind(this),
				error: this._onSubmitError.bind(this)
			});
		},
		onCancel: function () {
			this._resetModel();
			this.onBack();
		},
		_resetModel: function () {
			if (this._oObjectId === "new") {
				this.oModel.deleteCreatedEntry(this._oNewContext);
			} else {
				this.oModel.resetChanges();
			}
		},
		_onSubmitSuccess: function (oResponse) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.success(
				"Question created successfully.", {
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
			this.onCancel();
		},
		_onSubmitError: function (oError) {
			debugger;
			MessageBox.error("Question creation error.");
		}

	});

});