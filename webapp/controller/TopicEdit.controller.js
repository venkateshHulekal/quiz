sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (Controller, History, MessageBox) {
	"use strict";

	return Controller.extend("com.cg.coe.quizmaster.zquizmaster.controller.TopicEdit", {

		onInit: function () {
			this.oModel = this.getOwnerComponent().getModel();
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getRoute("TopicEdit").attachPatternMatched(this._onObjectMatched, this);
		},
		onBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this._oRouter.navTo("TargetView1", true);
			}
		},
		_onObjectMatched: function (oEvent) {
			debugger;
			this._oObjectId = oEvent.getParameter("arguments").objectId;
			if (this._oObjectId === "new") {
				this.oModel.setDeferredBatchGroups(["createGroup"]);

				this._oNewContext = this.oModel.createEntry("/QUIZTAGS", {
					groupId: "createGroup",
					properties: {
						id: "0",
						tagName: ""
					}
				});
				this._oObjectPath = this._oNewContext.sPath;
				//this.getView().setBindingContext(this._oNewContext);
				this.getView().bindElement({
					path: this._oObjectPath
				});
			} else {
				this._oObjectPath = "/QUIZTAGS('" + this._oObjectId + "')";
				this.getView().bindElement({
					path: this._oObjectPath
				});
			}
		},
		onTopicSave: function () {
			this.oModel.submitChanges({
				groupId: "createGroup",
				success: this._onSubmitSuccess.bind(this),
				error: this._onSubmitError.bind(this)
			});
		},
		onTopicCancel: function () {
			this._resetModel();
			this.onBack();
		},
		_resetModel: function () {
			if (this._oObjectId === "new") {
				this.oModel.deleteCreatedEntry(this._oNewContext);
			} else {
				this.oModel.resetChanges();
			}
		},
		_onSubmitSuccess: function (oResponse) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.success(
				"Topic created successfully.", {
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
			this.onTopicCancel();
		},
		_onSubmitError: function (oError) {
			MessageBox.error("Topic creation error.");
		}

	});

});